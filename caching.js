const uniqSort = function(arr) {
    const breadcrumbs = {};
    const result = new Array();
    for(let i = 0; i < arr.length; i++){
      if(!breadcrumbs[arr[i]]){
        result.push(arr[i]);
        breadcrumbs[arr[i]] = true;
      }
    }
    result.sort((a, b) => a - b);
    return result;
};

uniqSort([4,2,2,3,2,2,2]); // => [2,3,4]


//Memoization
const times10 = (n) => {return 10*n};

const cache = {};

const memoTimes10 = (n) => {
  if(n in cache){
    return cache[n]
  }
  cache[n] = times10(n);
  return times10(n);
}

console.log('Task 2 calculated value:', memoTimes10(9));	// calculated
console.log('Task 2 cached value:', memoTimes10(9));	// cached

const memoizedClosureTimes10 = (n) => {
    const cache = {};
    console.log(cache);
    return function(n) {
      if(n in cache){
        return cache[n]
      }
      cache[n] = times10(n);
      return times10(n);
    }
  }
  
  const memoClosureTimes10 = memoizedClosureTimes10();
  console.log('Task 3 cached value:', memoClosureTimes10(9));
  console.log('Task 3 cached value:', memoClosureTimes10(9));

  const memoizedClosureTimes10New = (cb) => {
    const cache = {};
    console.log(cache);
    return function(n) {
      if(n in cache){
        return cache[n]
      }
      cache[n] = cb(n);
      return cb(n);
    }
  }
  
  const memoClosureTimes10New = memoizedClosureTimes10New(times10);
  console.log('Task 3 cached value:', memoClosureTimes10New(9));
  console.log('Task 3 cached value:', memoClosureTimes10New(9));

  //Recursion

  const fact = (n) => {
    let result = 0;
    if(n === 0){
      return 1;
    } else {
      return fact(n - 1) * n;
    }
  }
  console.log(fact(5))

  const memoize = (fn) => {
      const cache = {};
      console.log(cache);
      return function(n) {
        if(n in cache){
          return cache[n]
        }
        cache[n] = cb(n);
        return cb(n);
      }
  }

  const factorial = memoize(
      (x) => {
        if(x === 0){
            return 1;
          } else {
            return factorial(x - 1) * x;
          }
      }
  )